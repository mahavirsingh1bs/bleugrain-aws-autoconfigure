package com.bleugrain.aws.autoconfigure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.carefarm.prakrati.security.Encryptor;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(AmazonS3Client.class)
@EnableConfigurationProperties(AwsS3Properties.class)
@Slf4j
public class BleugrainAwsAutoConfiguration {
	
	@Autowired
	private AwsS3Properties properties;
	
	@Autowired
	private Encryptor encryptor;
	
	@Bean
	@ConditionalOnMissingBean
    public AWSCredentials awsS3Credentials() {
		String accessKey = encryptor.decrypt(properties.getAccessKey());
		String secretKey = encryptor.decrypt(properties.getSecretKey());
        return new BasicAWSCredentials(accessKey, secretKey);
    }
    
    @Bean
    @ConditionalOnMissingBean
    public AmazonS3Client amazonS3Client(AWSCredentials awsS3Credentials) {
    	log.info("definign aws s3 client bean");
        AmazonS3Client amazonS3Client = new AmazonS3Client(awsS3Credentials);
        amazonS3Client.setRegion(Region.getRegion(Regions.fromName(properties.getRegion())));
        return amazonS3Client;
    }
	
}
