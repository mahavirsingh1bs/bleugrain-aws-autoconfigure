package com.bleugrain.aws.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "bleugrain.aws.s3")
public class AwsS3Properties {
	private String accessKey;
	private String secretKey;
	private String region;
	
}
